from django.contrib.auth.models import User,Permission
from .models import Cliente,Proveedor,Ingrediente,Pedidoproveedor,Producto,IngProd,Pedidocli,ProductosPedido
from django.contrib.auth import authenticate,login,logout,hashers
from django.shortcuts import render,redirect
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse
from django.forms import formset_factory, inlineformset_factory
from django.forms.models import modelformset_factory
from .forms import PedidoprovFormSet,PedidoProveedorForm,IngredienteProductoForm,PedidocliForm,prodPedidoForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.db.models import Min,Max,Avg,Sum
from django.core.files.storage import FileSystemStorage
from dateutil.parser import parse
# Create your views here.

user = authenticate(username='admin@gmail.com', password='admin')
if user is None:
    user = User.objects.create_user(username='admin@gmail.com', password='admin')
    p1 = Permission.objects.get(codename='is_admin')
    user.user_permissions.add(p1)
    user.save()
def home(request):
    user = request.user
    lista = request.session["carrito_de_compra"]
    print(request)
    context={}
    if user.has_perm('home.is_admin'):
        print("soy admin coñaso")
        context['isAdmin'] = True
    if user.has_perm('home.is_user'):
        context['isAdmin'] = False

    if request.method == 'POST':
        logout(request)
        return redirect('InicioLogin')
    return render(request, 'home/home.html',{'context':context,'lista':lista})


def new_user(request):

    context = {

    }
    if request.method == 'POST':
        usuario = request.POST['email']
        nombre = request.POST['nombre']
        apellido1 = request.POST['apellido1']
        apellido2 = request.POST['apellido2']
        razon_social = request.POST['razon_social']
        cif = request.POST['cif']
        telefono = request.POST['telefono1']
        telefono2 =request.POST['telefono2']
        paginaweb = request.POST['paginaweb']
        password = request.POST['clave']
        repeatpassword = request.POST['clave2']
        tipo_cliente = request.POST['tipo_cliente']
        alergenos = request.POST['alergenos']
        necesidades_lim = request.POST['necesidades_lim']
        rol = request.POST['rol']
        print(usuario, apellido1,tipo_cliente)
        if password != repeatpassword:
            context['error_pass'] = True
            return render(request,'home/new_user.html',context)

        user = User.objects.create_user(usuario,usuario,password)
        if rol == 'ROLE_USER':
            p1 = Permission.objects.get(codename='is_user')
            user.user_permissions.add(p1)
        user.save()

        cliente= Cliente.objects.create(usuario=usuario,nombre=nombre,apellido1=apellido1,apellido2=apellido2,razon_social=razon_social,cif=cif,telefono=telefono,telefono2=telefono2,paginaweb=paginaweb,password=password,tipo_cliente=tipo_cliente,alergenos=alergenos,necesidades_lim=necesidades_lim,rol=rol)
        cliente.save()
        context['formulario_correcto'] = True
        context['user'] = nombre
        return render(request,'home/new_user.html',context)

    return render(request, 'home/new_user.html', context)



def user_login(request):
    context={}
    if request.method == 'POST':
        name = request.POST['username']
        password = request.POST['passwordid']
        user = authenticate(request,username=name,password=password)
        exitopass =''
        exitouser =''
        try:
            usercheck = User.objects.get(username=name)
            print(usercheck)
            exitopass = usercheck.check_password(password)
            exitouser = True
        except User.DoesNotExist:
            exitopass = False
            exitouser = False

        print("VMOS a ver el valor de exito",exitopass,exitouser)

        if user is None:
            """Usuario existe. Compruebo pass"""
            if exitopass == False and exitouser == True:
                context['exitopass'] = False
                return render(request, 'home/login.html', context)
            print("usuario no existe")
            context['noexiste'] = True
            return render(request,'home/login.html',context)
        print("TODO bien parce")
        if user is not None:
            login(request,user)
            request.session["carrito_de_compra"] = []
            if user.is_active:
                return redirect('home')
        else:
            context['invalid'] = True
            return render(request,'home/login.html',context)
    else:
        return render(request,'home/login.html',context)

def administration(request):
    return render(request, 'home/administracion.html')

def administration_user(request):
    context={}
    context['usuarios'] = Cliente.objects.all().values()
    context['mama'] =  User.objects.all().values()

    informacionUsuario = request.GET.get('nombrecompleto')
    if (informacionUsuario is not None and informacionUsuario is not ''):
        context['users'] = Cliente.objects.filter(nombre__contains=informacionUsuario).values()
        context['lleno'] = True
    else:
        context['lleno'] = False

    return render(request,'administracion/usuarios.html',context)

def administration_editUser(request):
    context={}
    if request.method == 'GET':
        usuario = request.GET.get('usuario')
        password = request.GET.get('password')
        nombre = request.GET.get('nombre')
        apellido1 = request.GET.get('apellido1')
        apellido2 = request.GET.get('apellido2')
        telefono = request.GET.get('telefono')
        alergenos = request.GET.get('alergenos')
        necesidades = request.GET.get('necesidades')
        tipo_cliente = request.GET.get('tipo_cliente')
        rol = request.GET.get('rol')
        context['usuario'] = usuario
        context['password'] = password
        context['nombre'] = nombre
        context['apellido1'] = apellido1
        context['apellido2'] = apellido2
        context['usuario'] = usuario
        context['telefono'] = telefono
        context['alergenos'] = alergenos
        context['necesidades'] = necesidades
        context['tipo_cliente'] = tipo_cliente
        context['rol'] = rol
        print("Viendo password:", password)
        return render(request, 'administracion/usuarios/editUser.html', context)

    if request.method == 'POST':
        """Datos para autenticacion"""
        usuario = request.POST.get('usuario')
        password = request.POST.get('password')
        """Datos a insertar en BBDD"""
        nombre = request.POST.get('nombre')
        apellido1 = request.POST.get('apellido1')
        apellido2 = request.POST.get('apellido2')
        telefono = request.POST.get('telefono')
        tipo_cliente = request.POST.get('tipo_cliente')
        alergenos = request.POST.get('alergenos')
        necesidades_lim = request.POST.get('necesidades_lim')
        rol = request.POST.get('rol')


        user = authenticate(request, username=usuario, password=password)
        if user is not None:
            Cliente.objects.select_related().filter(usuario__exact=usuario).update(nombre=nombre, apellido1=apellido1,
                                                                                   apellido2=apellido2,
                                                                                   telefono=telefono,
                                                                                   tipo_cliente=tipo_cliente,
                                                                                   alergenos=alergenos,
                                                                                   necesidades_lim=necesidades_lim,
                                                                                   rol=rol)
            userPermissions = User.objects.get(username=usuario)
            if rol == 'ROLE_USER':
                p1 = Permission.objects.get(codename='is_user')
                userPermissions.user_permissions.clear()
                userPermissions.user_permissions.add(p1)
            if rol == 'ROLE_ADMIN':
                p1 = Permission.objects.get(codename='is_admin')
                userPermissions.user_permissions.clear()
                userPermissions.user_permissions.add(p1)

            userPermissions.save()

        return redirect('AdministracionUser')

    return render(request,'administracion/usuarios.html',context)

def editProfile(request):
    context = {}
    user = request.user
    infoCliente = Cliente.objects.filter(usuario__contains=user).values()
    for val in infoCliente:
        context['nombre'] = val['nombre']
        context['apellido1'] = val['apellido1']
        context['apellido2'] = val['apellido2']
        context['paginaweb'] = val['paginaweb']
        context['telefono2'] = val['telefono2']
        context['rol'] = val['rol']
        context['usuario'] = val['usuario']
        context['razon_social'] = val['razon_social']
        context['cif'] = val['cif']
        context['telefono'] = val['telefono']
        context['tipo_cliente'] = val['tipo_cliente']
        context['alergenos'] = val['alergenos']
        context['necesidades_lim'] = val['necesidades_lim']
        context['password'] = val['password']

    if request.method == 'POST':

        nombre = request.POST['nombre']
        apellido1 = request.POST['apellido1']
        apellido2 = request.POST['apellido2']
        telefono = request.POST['telefono']
        telefono2 = request.POST['telefono2']
        paginaweb = request.POST['paginaweb']
        razon_social = request.POST['razon_social']
        cif = request.POST['cif']
        tipo_cliente = request.POST['tipo_cliente']
        alergenos = request.POST['alergenos']
        necesidades_lim = request.POST['necesidades_lim']


        if user is not None:
            Cliente.objects.select_related().filter(usuario__exact=user).update(nombre=nombre, apellido1=apellido1,
                                                                                   apellido2=apellido2,
                                                                                   telefono=telefono,
                                                                                   telefono2=telefono2,
                                                                                   tipo_cliente=tipo_cliente,
                                                                                   alergenos=alergenos,
                                                                                   paginaweb=paginaweb,
                                                                                   razon_social=razon_social,
                                                                                   necesidades_lim=necesidades_lim,
                                                                                   cif=cif)




            return redirect('PerfilUser')


    return render(request,'home/perfil.html',context)

def change_password(request):
    user = request.user
    if request.method == 'POST':

        form = PasswordChangeForm(data=request.POST, user=request.user)
        aux = {'form': form}
        if form.is_valid():
            password = request.POST['new_password1']
            print(password)
            Cliente.objects.select_related().filter(usuario__exact=user).update(password=password)

            form.save()
            update_session_auth_hash(request, form.user)
            return redirect(reverse('home'))
        else:
            print("hubo un error")
            return render(request, 'home/change_password.html', aux)
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'home/change_password.html', args)

def administration_prov(request):
    if request.method == 'GET':
        aux = request.GET.get('eliminar')
        if aux == 'true':
            cif = request.GET['cif_proveedor']
            print(cif)
            dao = Proveedor.objects.get(cif_proveedor=cif)
            dao.delete()
            return redirect('AdministracionProveedor')
        else:
            context = {}
            context['proveedores'] = Proveedor.objects.all().values()
            return render(request, 'administracion/proveedores.html', context)

    if request.method == 'POST':
        nombre = request.POST['nombre']
        nombre_com = request.POST['nombre_comercial']
        direccion = request.POST['direccion']
        telefono = request.POST['telefono']
        email = request.POST['email']
        enlace = request.POST['enlace']
        CIF_Proveedor = request.POST['CIF']


        Proveedor.objects.create(nombre=nombre, nombre_com=nombre_com,direccion=direccion,
                                                                          telefono=telefono, email=email,enlace=enlace,
                                                                            cif_proveedor=CIF_Proveedor)
        return redirect('AdministracionProveedor')

    return render(request,'administracion/proveedores.html')

def administration_editProv(request):
    context = {}
    if request.method == 'GET':
        nombre = request.GET.get('nombre')
        nombre_com = request.GET.get('nombre_com')
        direccion = request.GET.get('direccion')
        telefono = request.GET.get('telefono')
        email = request.GET.get('email')
        enlace = request.GET.get('enlace')
        cif_proveedor = request.GET.get('cif_proveedor')
        prefix_id = request.GET.get('prefix_id')
        context['nombre'] = nombre
        context['nombre_com'] = nombre_com
        context['direccion'] = direccion
        context['telefono'] = telefono
        context['email'] = email
        context['enlace'] =enlace
        context['cif_proveedor'] = cif_proveedor
        context['prefix_id'] = prefix_id
        return render(request, 'administracion/proveedores/editProveedor.html', context)

    if request.method == 'POST':

        """Datos a insertar en BBDD"""
        nombre = request.POST.get('nombre')
        nombre_com = request.POST.get('nombre_com')
        direccion = request.POST.get('direccion')
        telefono = request.POST.get('telefono')
        email = request.POST.get('email')
        enlace = request.POST.get('enlace')
        cif_proveedor = request.POST.get('cif_proveedor')
        print("Dentro del formulario POST",cif_proveedor,enlace,nombre,nombre_com,direccion,telefono,email)

        Proveedor.objects.select_related().filter(cif_proveedor__exact=cif_proveedor).update(nombre=nombre, nombre_com=nombre_com,
                                                                                   direccion=direccion, telefono = telefono, enlace = enlace,
                                                                                   email = email)

        return redirect('AdministracionProveedor')

    return render(request, 'administracion/proveedores.html', context)


def AdminProductosProv(request):

    return render(request, 'administracion/productos_proveedores.html')

def AltaIngrediente(request):
    context={}
    if request.method == 'POST':
        """Datos a insertar en BBDD"""
        nombre = request.POST.get('nombre')
        marca = request.POST.get('marca')
        alergenos = request.POST.get('alergenos')
        tipo_ingrediente = request.POST.get('tipo_ingrediente')

        print("Dentro del formulario POST",nombre,marca,alergenos,tipo_ingrediente)
        Ingrediente.objects.create(nombre=nombre, marca=marca, alergenos=alergenos,tipo_ingrediente=tipo_ingrediente)
        """ Obtenemos el ultimo valor isnertado"""
        lastId = Ingrediente.objects.latest('id_ingrediente')
        id_ingre = lastId.id_ingrediente
        context['ingrediente'] = Ingrediente.objects.get(id_ingrediente=id_ingre)
        context['usuarios'] = Proveedor.objects.all().values()

        """Creamos formulario de la tabla PedidoProveedor"""
        pedidoProvFormSet = formset_factory(PedidoProveedorForm)
        formset = pedidoProvFormSet()
        return render(request, 'administracion/productos_proveedores/alta/tablaprov.html',{'context': context,'formset':formset})

    return render(request, 'administracion/productos_proveedores/alta.html')

def AltaIngredienteProv(request):
    context = {}
    pedidoProvFormSet = formset_factory(PedidoProveedorForm)
    if request.method == 'POST':
        formset = pedidoProvFormSet(request.POST, request.FILES)
        print("Form", formset)
        if formset.is_valid():

            for ingreprov in formset:
                ingreprov.save()
            messages.success(request,'Ingrediente Registrado en el Proveedor seleccionado')
        else:
            print("something were wrong")

    lastId = Ingrediente.objects.latest('id_ingrediente')
    id_ingre = lastId.id_ingrediente
    context['ingrediente'] = Ingrediente.objects.get(id_ingrediente=id_ingre)
    formset = pedidoProvFormSet()
    context['usuarios'] = Proveedor.objects.all().values()
    return render(request, 'administracion/productos_proveedores/alta/tablaprov.html',
                      {'formset': formset, 'context': context})



def bajaIngrediente(request):
    context = {}
    context['ingredientes'] = Ingrediente.objects.all().values()
    if request.method == 'POST':
        identificadorIngre = request.POST.get('id_ingrediente')
        print("Vamo a calmarno" ,identificadorIngre)
        ingreToDelete = Ingrediente.objects.get(id_ingrediente=identificadorIngre)
        ingreToDelete.delete()
        messages.success(request, 'Ingrediente con Identificador')
        context['idRemoved'] = identificadorIngre
        return render(request, 'administracion/productos_proveedores/baja.html', context)

    return render(request, 'administracion/productos_proveedores/baja.html',context)

def modifyIngredient(request):
    context = {}
    context['ingredientes'] = Ingrediente.objects.all().values()


    return render(request, 'administracion/productos_proveedores/ingredientes.html',context)

def modifyIngrediente(request):

    """ context = {}
    context['ingredientes'] = Ingrediente.objects.all().values()"""
    context={}

    if request.method == 'POST':
        IngredienteId = request.POST.get('id_identificador')
        identificadorIngre = IngredienteId
        ingre = Ingrediente.objects.get(pk=identificadorIngre)
        PedidoProvModifyFormSet = inlineformset_factory(Ingrediente, Pedidoproveedor,
                                                        fk_name='id_ingrediente',
                                                        fields=('cif_proveedor', 'cantidad', 'precio', 'id'), extra=0)

        formset = PedidoProvModifyFormSet(request.POST, request.FILES,instance=ingre)
        print(formset)
        if formset.is_valid():
            for ingreprov in formset:
                ingreprov.save()
        return redirect('modifyIngredient')
    else:

        IngredienteId = request.GET.get('id_ingrediente')
        context['idIngrediente'] = IngredienteId
        identificadorIngre = IngredienteId
        ingre = Ingrediente.objects.get(pk=identificadorIngre)

        PedidoProvModifyFormSet = inlineformset_factory(Ingrediente, Pedidoproveedor,
                                                        fk_name='id_ingrediente',
                                                        fields=('cif_proveedor', 'cantidad', 'precio', 'id'), extra=0)

        formset = PedidoProvModifyFormSet(instance=ingre)
    return render(request, 'administracion/productos_proveedores/ingredientes/modificar.html',{'formset': formset, 'context':context})

def provModifyIng(request):
    context = {}
    if request.method == 'GET':
        nombre = request.GET.get('nombre')
        marca = request.GET.get('marca')
        alergenos = request.GET.get('alergenos')
        tipo_ingrediente = request.GET.get('tipo_ingrediente')
        identificadoring = request.GET.get('identificadoring')
        id_ingrediente = request.GET.get('id_ingrediente')
        context['nombre'] = nombre
        context['marca'] = marca
        context['alergenos'] = alergenos
        context['tipo_ingrediente'] = tipo_ingrediente
        context['identificadoring'] = identificadoring
        context['id_ingrediente'] = id_ingrediente

    if request.method == 'POST':
        """Datos a insertar en BBDD"""
        nombre = request.POST.get('nombre')
        marca = request.POST.get('marca')
        alergenos = request.POST.get('alergenos')
        tipo_ingrediente = request.POST.get('tipo_ingrediente')
        id_ingrediente = request.POST.get('id_ingrediente')

        Ingrediente.objects.select_related().filter(id_ingrediente__exact=id_ingrediente).update(nombre=nombre,marca=marca,
                                                                                                 alergenos=alergenos,
                                                                                                 tipo_ingrediente=tipo_ingrediente)

        return redirect('modifyIngredient')

    return render(request, 'administracion/productos_proveedores/ingredientes/editarIngre.html',context)


def IngrProvTable(request):
    print("REALIZANDO CONSULTA")
    """ pe= Pedidoproveedor.objects.get(id_ingrediente__marca="Sugaro")"""
    ingresYprov = Pedidoproveedor.objects.values('id_ingrediente__identificadoring','id_ingrediente__id_ingrediente','id_ingrediente__nombre',
                                                 'id_ingrediente__marca','id_ingrediente__alergenos',
                                                 'id_ingrediente__tipo_ingrediente','cif_proveedor__nombre','cif_proveedor__cif_proveedor',
                                                 'cif_proveedor__prefix_id','cantidad','precio').all().order_by('id_ingrediente__tipo_ingrediente')


    return render(request, 'administracion/productos_proveedores/tablaIngrProv.html',{'data':ingresYprov})


def IngrProvTableRentable(request):
    print("MIN VALUES AND GROUP BY")
    ingrentables = Pedidoproveedor.objects.values('id_ingrediente__nombre').annotate(min_p=Min('precio'),
                                                                                     prov_cif=Max(
                                                                                         'cif_proveedor__cif_proveedor'),
                                                                                     prov_nombre=Max(
                                                                                         'cif_proveedor__nombre'),
                                                                                     prov_id=Max(
                                                                                         'cif_proveedor__prefix_id'),
                                                                                     ingre_id=Max(
                                                                                         'id_ingrediente__id_ingrediente'),
                                                                                     ingre_alergeno=Max(
                                                                                         'id_ingrediente__alergenos'),
                                                                                     ingre_tipo=Max(
                                                                                         'id_ingrediente__tipo_ingrediente'),
                                                                                     ingre_marca=Max(
                                                                                         'id_ingrediente__marca'),
                                                                                     ingre_cantidad=Max('cantidad')
                                                                                     ).order_by('id_ingrediente__tipo_ingrediente')

    return render(request, 'administracion/productos_proveedores/tablaIngrProv/Ingredientes_Rentables.html',
                  {'data': ingrentables})


def ingredientStock(request):
    ingredientes = Ingrediente.objects.all().values().order_by('tipo_ingrediente')

    return render(request, 'administracion/productos_proveedores/stock.html',{'ingredientes':ingredientes})


def ingredientStockEdit(request):
    context = {}
    if request.method == 'GET':
        nombre = request.GET.get('nombre')
        marca = request.GET.get('marca')
        alergenos = request.GET.get('alergenos')
        tipo_ingrediente = request.GET.get('tipo_ingrediente')
        id_ingrediente = request.GET.get('id_ingrediente')
        stock = request.GET.get('stock')
        cantidad_stock = request.GET.get('cantidad_stock')
        context['nombre'] = nombre
        context['marca'] = marca
        context['alergenos'] = alergenos
        context['tipo_ingrediente'] = tipo_ingrediente
        context['id_ingrediente'] = id_ingrediente
        context['stock'] = stock
        context['cantidad_stock'] = cantidad_stock
        return render(request, 'administracion/productos_proveedores/stock/editarStock.html',context)
    if request.method == 'POST':
        """Datos a insertar en BBDD - tabla Ingredientes"""
        nombre = request.POST.get('nombre')
        marca = request.POST.get('marca')
        alergenos = request.POST.get('alergenos')
        tipo_ingrediente = request.POST.get('tipo_ingrediente')
        id_ingrediente = request.POST.get('id_ingrediente')
        stock = request.POST.get('stock')
        cantidad_stock = request.POST.get('cantidad_stock')

        Ingrediente.objects.select_related().filter(id_ingrediente__exact=id_ingrediente).update(nombre=nombre,
                                                                                                 marca=marca,
                                                                                                 alergenos=alergenos,
                                                                                                 tipo_ingrediente=tipo_ingrediente,
                                                                                                 stock=stock,
                                                                                                 cantidad_stock=cantidad_stock)

        return redirect('ingredientStock')

    return redirect('ingredientStock')

def adminProductosOfertados(request):
    return render(request, 'administracion/productos_ofertados.html')

def adminProdofferAlta(request):
    context = {}
    if request.method == 'POST' and request.FILES['image']:
        """Datos a insertar en BBDD"""
        nombre = request.POST.get('nombre')
        alergenos_producto = request.POST.get('alergenos_producto')
        tipo_producto = request.POST.get('tipo_producto')
        precio = request.POST.get('precio')
        descripcion = request.POST.get('descripcion')
        myFile = request.FILES['image']
        fs=FileSystemStorage()
        filename=fs.save(myFile.name,myFile)
        uploaded_file_url = fs.url(filename)
        print(uploaded_file_url)
        print("Dentro del formulario POST")
        Producto.objects.create(nombre=nombre, alergenos_producto=alergenos_producto, tipo_producto=tipo_producto, precio=precio, coste=0,descripcion=descripcion,image=uploaded_file_url)
        lastId = Producto.objects.latest('id_producto')
        id_producto = lastId.id_producto
        context['producto'] = Producto.objects.get(id_producto=id_producto)
        context['ingrediente'] = Ingrediente.objects.all().values()
        ingreprodFormSet = formset_factory(IngredienteProductoForm)
        formset = ingreprodFormSet()
        return render(request, 'administracion/productos_ofertados/alta_producto_step1/alta_producto_step2.html',
                      {'context': context, 'formset': formset})

    return render(request, 'administracion/productos_ofertados/alta_producto_step1.html')

def adminProdofferAlta2(request):
    context = {}
    ingreprodFormSet = formset_factory(IngredienteProductoForm)
    ingredienteID=''
    if request.method == 'POST':
        formset = ingreprodFormSet(request.POST, request.FILES)

        if formset.is_valid():
            ingredienteID = request.POST.get('form-0-id_ingrediente')

            for ingrepro in formset:
                ingrepro.save()
            messages.success(request, 'Ingrediente añadido en el Producto dado de alta')
        else:
            print("something were wrong")
    """Volvemos a coger el ultimo prod para que siga añadiendo ingredientes"""
    lastId = Producto.objects.latest('id_producto')
    id_producto = lastId.id_producto
    formset = ingreprodFormSet()
    context['ingrediente'] = Ingrediente.objects.all().values()
    """Empezmos con el coste cálculo"""
    costeProveedor = Pedidoproveedor.objects.filter(id_ingrediente=ingredienteID).aggregate(media=Avg('precio'))
    cProv = costeProveedor['media']
    costeProducto = Producto.objects.filter(id_producto=id_producto).aggregate(suma=Sum('coste'))
    cProd = costeProducto['suma']
    showCoste = ''

    if cProd != 0:
        """Hay mas ingredientes para el producto por lo tanto calculamos nueva media"""

        suma = float(cProd) + float(cProv)
        mediaNueva = suma / 2
        Producto.objects.select_related().filter(id_producto__exact=id_producto).update(coste=mediaNueva)
        showCoste = mediaNueva


    else:
        """Es 0 -> primer ingrediente para el producto"""
        Producto.objects.select_related().filter(id_producto__exact=id_producto).update(coste=cProv)
        showCoste = cProv
    context['showCoste'] = showCoste
    context['producto'] = Producto.objects.get(id_producto=id_producto)

    return render(request, 'administracion/productos_ofertados/alta_producto_step1/alta_producto_step2.html',
                  {'formset': formset, 'context': context})


def adminProdofferBaja(request):
    context = {}
    context['productos_ofertados'] = Producto.objects.all().values()
    print(context)
    if request.method == 'POST':
        idProducto = request.POST.get('id_producto')
        prodToDelete = Producto.objects.get(id_producto=idProducto)
        prodToDelete.delete()
        messages.success(request, 'Producto con Identificador')
        context['idRemoved'] = idProducto
        return render(request, 'administracion/productos_ofertados/baja.html', context)

    return render(request, 'administracion/productos_ofertados/baja.html', context)

def adminProdofferModify(request):
    context = {}
    context['productsOffer'] = Producto.objects.all().values()

    return render(request, 'administracion/productos_ofertados/modificar.html',context)
def adminProdofferModifyStep1(request):
    context = {}
    if request.method == 'GET':
        id_producto = request.GET.get('id_producto')
        nombre = request.GET.get('nombre')
        coste = request.GET.get('coste')
        precio = request.GET.get('precio')
        tipo_producto = request.GET.get('tipo_producto')
        alergenos_producto = request.GET.get('alergenos_producto')
        descripcion = request.GET.get('descripcion')
        context['id_producto'] = id_producto
        context['nombre'] = nombre
        context['coste'] = coste
        context['precio'] = precio
        context['tipo_producto'] = tipo_producto
        context['alergenos_producto'] = alergenos_producto
        context['descripcion'] = descripcion
        return render(request, 'administracion/productos_ofertados/modificar/modificar_step1.html', context)

    if request.method == 'POST':
        """Datos a insertar en BBDD """
        id_producto = request.POST.get('id_producto')
        nombre = request.POST.get('nombre')
        tipo_producto = request.POST.get('tipo_producto')
        alergenos_producto = request.POST.get('alergenos_producto')
        coste = request.POST.get('coste')
        precio = request.POST.get('precio')
        descripcion = request.POST.get('descripcion')

        Producto.objects.select_related().filter(id_producto__exact=id_producto).update(nombre=nombre,
                                                                                        tipo_producto=tipo_producto,
                                                                                        alergenos_producto=alergenos_producto,
                                                                                        coste=coste,
                                                                                        precio=precio,
                                                                                        descripcion=descripcion)

        return redirect('adminProdofferModify')

    return render(request, 'administracion/productos_ofertados/modificar/modificar_step1.html')

def adminProdofferModifyStep2(request):
    """ Obtenemos el ultimo alor isnertado"""
    if request.method == 'GET':
        context={}
        id_producto = request.GET.get('id_producto')
        print(id_producto)
        context['prod_ing'] = IngProd.objects.filter(id_producto=id_producto).values()

        print(context['prod_ing'])
        context['producto'] = Producto.objects.get(id_producto=id_producto)
        context['ingrediente'] = Ingrediente.objects.all().values()
        """Creamos formulario de la tabla INGPROD"""
        ingreprodFormSet = formset_factory(IngredienteProductoForm)
        formset = ingreprodFormSet()
        return render(request, 'administracion/productos_ofertados/modificar/modificar_step2.html',
                      {'context': context, 'formset': formset})

    if request.method == 'POST':
        id_prod = request.POST.get('form-0-id_producto')
        context ={}
        ingreprodFormSet = formset_factory(IngredienteProductoForm)
        ingredienteID = ''
        formset = ingreprodFormSet(request.POST, request.FILES)
        if formset.is_valid():
            ingredienteID = request.POST.get('form-0-id_ingrediente')
            for ingrepro in formset:
                ingrepro.save()
            messages.success(request, 'Ingrediente añadido en el Producto dado de alta')
        else:
            print("something were wrong")

        formset = ingreprodFormSet()
        context['ingrediente'] = Ingrediente.objects.all().values()
        costeProveedor = Pedidoproveedor.objects.filter(id_ingrediente=ingredienteID).aggregate(media=Avg('precio'))
        cProv = costeProveedor['media']
        costeProducto = Producto.objects.filter(id_producto=id_prod).aggregate(suma=Sum('coste'))
        cProd = costeProducto['suma']
        showCoste = ''

        if cProd != 0:

            suma = float(cProd) + float(cProv)
            mediaNueva = suma / 2
            Producto.objects.select_related().filter(id_producto__exact=id_prod).update(coste=mediaNueva)
            showCoste = mediaNueva

        else:
            Producto.objects.select_related().filter(id_producto__exact=id_prod).update(coste=cProv)
            showCoste = cProv
        context['showCoste'] = showCoste
        context['prod_ing'] = IngProd.objects.filter(id_producto=id_prod).values()
        context['producto'] = Producto.objects.get(id_producto=id_prod)

        return render(request, 'administracion/productos_ofertados/modificar/modificar_step2.html',
                          {'formset': formset, 'context': context})
        """return render(request, 'administracion/productos_ofertados/modificar/modificar_step2.html')"""
    return render(request, 'administracion/productos_ofertados/modificar/modificar_step2.html')

"""Productos - coffee breaks, principales postres y todos"""
def clientesCatering(request):
    lista = request.session["carrito_de_compra"]
    return render(request, 'home/catering.html',{'lista':lista})

def clientesProductos(request):
    print(request.session.session_key)
    lista = request.session["carrito_de_compra"]
    return render(request, 'home/catering/productos.html',{'lista':lista})


def clientProdCoffeBreaks(request):
    if request.method == 'GET':

        print(request.session.session_key)
        lista = request.session["carrito_de_compra"]
        print(lista)
        tipo_coffee = Producto.objects.filter(tipo_producto="coffee breaks").values()

        return render(request, 'home/catering/productos/coffee_breaks.html',{'tipo_coffee':tipo_coffee,'lista':lista})
    if request.method == 'POST':
        id_prod = request.POST.get('id_producto')
        lista = request.session["carrito_de_compra"]
        lista.append(id_prod)
        print(lista)
        request.session["carrito_de_compra"] = lista

        tipo_coffee = Producto.objects.filter(tipo_producto="coffee breaks").values()
        return render(request, 'home/catering/productos/coffee_breaks.html', {'tipo_coffee': tipo_coffee,'lista':lista})

def clientProdPrincipales(request):
    if request.method == 'GET':
        print(request.session.session_key)
        lista = request.session["carrito_de_compra"]
        print(lista)
        tipo_principales = Producto.objects.filter(tipo_producto="principales").values()

        return render(request, 'home/catering/productos/principales.html',
                      {'tipo_principales': tipo_principales, 'lista': lista})

    if request.method == 'POST':
        id_prod = request.POST.get('id_producto')
        lista = request.session["carrito_de_compra"]
        lista.append(id_prod)
        print(lista)
        request.session["carrito_de_compra"] = lista

        tipo_principales = Producto.objects.filter(tipo_producto="principales").values()
        return render(request, 'home/catering/productos/coffee_breaks.html', {'tipo_coffee': tipo_principales,'lista':lista})


def clientProdPostres(request):
    if request.method == 'GET':
        print(request.session.session_key)
        lista = request.session["carrito_de_compra"]
        print(lista)
        tipo_postres = Producto.objects.filter(tipo_producto="postres").values()

        return render(request, 'home/catering/productos/postres.html',
                      {'tipo_postres': tipo_postres, 'lista': lista})

    if request.method == 'POST':
        id_prod = request.POST.get('id_producto')
        lista = request.session["carrito_de_compra"]
        lista.append(id_prod)
        print(lista)
        request.session["carrito_de_compra"] = lista
        tipo_postres = Producto.objects.filter(tipo_producto="postres").values()
        return render(request, 'home/catering/productos/postres.html', {'tipo_postres': tipo_postres,'lista':lista})



def clientProdTodos(request):
    if request.method == 'GET':
        print(request.session.session_key)
        lista = request.session["carrito_de_compra"]
        print(lista)
        tipo_todos = Producto.objects.values()

        return render(request, 'home/catering/productos/todos.html',
                      {'tipo_todos': tipo_todos, 'lista': lista})

    if request.method == 'POST':
        id_prod = request.POST.get('id_producto')
        lista = request.session["carrito_de_compra"]
        lista.append(id_prod)
        print(lista)
        request.session["carrito_de_compra"] = lista
        tipo_todos = Producto.objects.values()
        return render(request, 'home/catering/productos/todos.html', {'tipo_todos': tipo_todos,'lista':lista})

"""Fin Productos"""

def clientProdPedido(request):

    lista = request.session["carrito_de_compra"]
    print(lista)
    productos = Producto.objects.filter(pk__in=lista)

    print(productos)

    return render(request, 'home/catering/productos/pedido.html',{'productos':productos,'lista':lista})

def cleanCarrito(request):
    if request.method =='GET':
        lista = request.session["carrito_de_compra"]
        lista.clear()
        print(lista)
        request.session["carrito_de_compra"] = lista

        return render(request, 'home/catering.html')

def clientProdPedidoStepEnd(request):

    if request.method == 'POST':
        user = request.user
        client = Cliente.objects.get(usuario=user)
        """Creamos el pedido para el cliente"""
        Pedidocli.objects.create(cliente=client)
        """Obtenemos ultimo id insertado"""
        lastID = Pedidocli.objects.latest('id')
        for i in request.POST:
            if i == "csrfmiddlewaretoken" or i == "orderTotal":
                print("no hago nada")
            else:
                print("metemos en BBDD el id_producto:",i)
                id_prod = Producto.objects.get(id_producto=i)
                print("id_prod",id_prod)
                ProductosPedido.objects.create(pedido=lastID,producto=id_prod,cantidad=request.POST.get(i))
                """Una vez insertado los productos lo que tengo que hacer es eliminar productos"""
                lista = request.session["carrito_de_compra"]
                lista.clear()
                print(lista)
                request.session["carrito_de_compra"] = lista

    return render(request, 'home/catering/productos/pedido/pedido_step_end.html')

"""Ver localizacion"""
def location(request):
    return render(request, 'home/localizacion.html')

"""Gestion de pedidos"""
def adminGestionPedidos(request):
    return render(request, 'administracion/gestion_pedidos.html')

def adminGestionPedidosAlta(request):
    context = {}
    pedidocliFormSet = formset_factory(PedidocliForm)
    if request.method == 'POST':
        formset = pedidocliFormSet(request.POST, request.FILES)
        print("Form",formset)
        if formset.is_valid():
            print("Formulario valido")

            for pedidocli in formset:
                pedidocli.save()

            context['pedido'] = Pedidocli.objects.latest('id')
            context['productos'] = Producto.objects.all().values()
            productosPedidoFormSet = formset_factory(prodPedidoForm)
            formset = productosPedidoFormSet()

            return render(request,'administracion/gestion_pedidos/gestion_pedidos_alta/gestion_pedidos_alta_Step2.html',{'formset':formset,'context':context})

    formset = pedidocliFormSet()
    return render(request, 'administracion/gestion_pedidos/gestion_pedidos_alta.html',{'formset':formset})

def adminGestionPedidosAltaStep2(request):
    context = {}
    context['pedido'] = Pedidocli.objects.latest('id')
    context['productos'] = Producto.objects.all().values()
    productosPedidoFormSet = formset_factory(prodPedidoForm)
    
    if request.method == 'POST':
        formset = productosPedidoFormSet(request.POST, request.FILES)
        print(formset)
        if formset.is_valid():
            print("FORMSET VALIDO")
            for prodped in formset:
                prodped.save()
            messages.success(request,'Producto añadido')
                        
            
    formset = productosPedidoFormSet()
    return render(request, 'administracion/gestion_pedidos/gestion_pedidos_alta/gestion_pedidos_alta_Step2.html',{'context':context,'formset':formset})


def adminGestionPedidosBaja(request):
    context = {}
    context['pedidoscli'] = Pedidocli.objects.all().values()
    if request.method == 'POST':
        id_pedido = request.POST.get('id_pedido')
        pedidoToDelete = Pedidocli.objects.get(id=id_pedido)
        pedidoToDelete.delete()
        messages.success(request,'Pedido con Identificador')
        context['idRemoved'] = id_pedido
        return render(request, 'administracion/gestion_pedidos/baja.html', {'context': context})

    return render(request, 'administracion/gestion_pedidos/baja.html',{'context':context})

def adminGestionPedidosShow(request):
    context = {}
    context['pedidoscli'] = Pedidocli.objects.all().values()
    return render(request, 'administracion/gestion_pedidos/tabla_pedidos.html',{'context':context})

def adminGestionPedidoModify(request):
    context = {}
    context['pedidoscli'] = Pedidocli.objects.all().values()
    return render(request, 'administracion/gestion_pedidos/modificar.html',{'context':context})

def adminGestionPedidoModifyStep1(request):
    context = {}
    if request.method == 'GET':
        context['pedido_id']= request.GET.get('pedido_id')
        context['fecha_pedido'] = request.GET.get('fecha_pedido')
        context['fecha_entrega'] = request.GET.get('fecha_entrega')
        context['lugar'] = request.GET.get('lugar')
        context['n_comensales'] = request.GET.get('n_comensales')
        context['t_evento'] = request.GET.get('t_evento')
        context['t_servicio'] = request.GET.get('t_servicio')
        context['requisitos'] = request.GET.get('requisitos')
        context['comentarios'] = request.GET.get('comentarios')
        context['cliente'] = request.GET.get('cliente_id')
        print(context)
        return render(request, 'administracion/gestion_pedidos/modificar/modificar_step1.html', context)

    if request.method == 'POST':
        pedido_id = request.POST.get('pedido_id')
        fecha_pedido = request.POST.get('fecha_pedido')
        fecha_entrega = request.POST.get('fecha_entrega')
        lugar = request.POST.get('lugar')
        n_comensales = request.POST.get('n_comensales')
        t_evento = request.POST.get('t_evento')
        t_servicio = request.POST.get('t_servicio')
        requisitos = request.POST.get('requisitos')
        comentarios = request.POST.get('comentarios')
        fecha_pedido = parse(fecha_pedido)
        Pedidocli.objects.select_related().filter(id__exact=pedido_id).update(fecha_pedido=fecha_pedido,
                                                    fecha_entrega=fecha_entrega,lugar=lugar,
                                                    n_comensales=n_comensales,t_evento=t_evento,
                                                    t_servicio=t_servicio,requisitos=requisitos,
                                                    comentarios=comentarios)
        return redirect('adminGestionPedidoModify')
    return render(request, 'administracion/gestion_pedidos/modificar/modificar_step1.html',{'context':context})

def adminGestionPedidoModifyStep2(request):
    context = {}
    if request.method == 'GET':
        pedido_id = request.GET.get('pedido_id')
        context['prodpedido'] = ProductosPedido.objects.filter(pedido=pedido_id).values()
        context['pedidos'] = Pedidocli.objects.get(id=pedido_id)
        context['productos'] = Producto.objects.all().values()
        """Creamos formulario de la tabla productos_pedido"""
        prodpedidoFormSet = formset_factory(prodPedidoForm)
        formset = prodpedidoFormSet()
        return render(request, 'administracion/gestion_pedidos/modificar/modificar_step2.html', {'context': context,'formset':formset})


    return render(request, 'administracion/gestion_pedidos/modificar/modificar_step2.html',{'context':context})

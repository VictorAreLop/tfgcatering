from django import forms
from .models import Cliente,Pedidoproveedor,Ingrediente,IngProd,ProductosPedido,Pedidocli
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.forms import inlineformset_factory
class RegistroModelForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    repeatpassword = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Cliente
        fields = ['nombre','apellido1','apellido2','razon_social','cif','telefono','telefono2','paginaweb','usuario','password','repeatpassword','tipo_cliente','alergenos','necesidades_lim']


    def clean_password(self):
        password = self.cleaned_data.get("password")
        return password
    def clean_repeatpassword(self):
        password = self.clean_password()
        rpassword = self.cleaned_data.get("repeatpassword")
        print(password,rpassword)
        if password != rpassword:
            raise forms.ValidationError("Las contraseñas no coinciden")
        return rpassword


class PedidoProveedorForm(forms.ModelForm):
    class Meta:
        model = Pedidoproveedor
        fields = ['id_ingrediente','cif_proveedor','cantidad','precio']


PedidoprovFormSet = forms.modelformset_factory(Pedidoproveedor,extra=5,max_num=5,fields=['id_ingrediente','cif_proveedor','factura','cantidad','precio'])


PedidoProvModifyFormSet = inlineformset_factory(Ingrediente, Pedidoproveedor,
                                                fk_name='id_ingrediente',
                                                fields=('cif_proveedor','cantidad','precio'))
class IngredienteProductoForm(forms.ModelForm):
    class Meta:
        model = IngProd
        fields = ['id_producto','id_ingrediente','cantidad']


class PedidocliForm(forms.ModelForm):
    class Meta:
        model = Pedidocli
        fields = ['cliente','fecha_entrega','lugar','n_comensales','t_evento',
                  't_servicio','requisitos','comentarios']


class prodPedidoForm(forms.ModelForm):
    class Meta:
        model = ProductosPedido
        fields = ['pedido','producto','cantidad']
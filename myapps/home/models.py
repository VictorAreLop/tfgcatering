from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.
class Cliente(models.Model):
    ROLES= (
        ('ROLE_USER','user'),
        ('ROLE_ADMIN','admin'))
    STATE = (
        ('ALTA','alta'),
        ('BAJA','baja'))

    usuario = models.CharField(primary_key=True, max_length=40)
    nombre = models.CharField(max_length=50)
    apellido1 = models.CharField(max_length=50, blank=None, null=False)
    apellido2 = models.CharField(max_length=50)
    razon_social = models.CharField(max_length=30, blank=True, null=True)
    cif = models.CharField(max_length=10, blank=True, null=True)
    telefono = models.CharField(max_length=12, blank=None, null=False)
    telefono2 = models.CharField(max_length=12, blank=True, null=True)
    paginaweb = models.CharField(max_length=12, blank=True, null=True)
    password = models.CharField(max_length=20, blank=True, null=True)
    tipo_cliente = models.CharField(max_length=50, blank=True, null=True)
    alergenos = models.CharField(max_length=50, blank=True, null=True)
    necesidades_lim = models.CharField(max_length=50, blank=True, null=True)
    rol = models.CharField(max_length=20, blank=True, null=True)


    class Meta:
        permissions= (
            ('is_user',_('Is user')),
            ('is_admin',_('Is admin')),
        )

    class Meta:
        managed = False
        db_table = 'CLIENTE'

class Proveedor(models.Model):
    nombre = models.CharField(max_length=30)
    nombre_com = models.CharField(max_length=30)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.CharField(max_length=12, blank=True, null=True)
    email = models.CharField(max_length=40, blank=True, null=True)
    enlace = models.CharField(max_length=45, blank=True, null=True)
    prefix = models.CharField(db_column='PREFIX', max_length=10)  # Field name made lowercase.
    cif_proveedor = models.CharField(db_column='CIF_Proveedor', primary_key=True,
                                     max_length=12)  # Field name made lowercase.
    prefix_id = models.CharField(db_column='prefix_ID', max_length=45, blank=True,
                                 null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROVEEDOR'

class Ingrediente(models.Model):
    id_ingrediente = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=20)
    marca = models.CharField(max_length=20, blank=True, null=True)
    alergenos = models.CharField(max_length=30, blank=True, null=True)
    tipo_ingrediente = models.CharField(max_length=30, blank=True, null=True)
    prefix = models.CharField(db_column='PREFIX', max_length=45, blank=True, null=True)  # Field name made lowercase.
    identificadoring = models.CharField(db_column='IdentificadorING', max_length=45, blank=True, null=True)
    stock = models.CharField(db_column='Stock', max_length=2, blank=True,
                             null=True)  # Field name made lowercase.
    cantidad_stock = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'INGREDIENTE'
        get_latest_by ='id_ingrediente'

class Facturaprov(models.Model):
    id_factura = models.CharField(primary_key=True, max_length=20)
    cif_proveedor = models.ForeignKey('Proveedor', models.DO_NOTHING, db_column='CIF_Proveedor')  # Field name made lowercase.
    fecha = models.CharField(max_length=10)
    concepto = models.CharField(max_length=40, blank=True, null=True)
    t_impuesto = models.CharField(max_length=10, blank=True, null=True)
    coste = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FACTURAPROV'


class Pedidoproveedor(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_ingrediente = models.ForeignKey(Ingrediente, models.DO_NOTHING, db_column='id_ingrediente')
    cif_proveedor = models.ForeignKey('Proveedor', models.DO_NOTHING, db_column='CIF_Proveedor')  # Field name made lowercase.
    factura = models.CharField(max_length=20)
    cantidad = models.IntegerField()
    precio = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'PEDIDOPROVEEDOR'


class Producto(models.Model):
    id_producto = models.PositiveIntegerField(primary_key=True)
    nombre = models.CharField(max_length=30)
    coste = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    precio = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    tipo_producto = models.CharField(max_length=45, blank=True, null=True)
    descripcion = models.CharField(max_length=230, blank=True, null=True)
    alergenos_producto = models.CharField(max_length=100, blank=True, null=True)
    image = models.FileField(null=True, blank=True)
    class Meta:
        managed = False
        db_table = 'PRODUCTO'


class IngProd(models.Model):
    id_ingrediente = models.ForeignKey(Ingrediente, models.DO_NOTHING, db_column='id_ingrediente')
    id_producto = models.ForeignKey('Producto', models.DO_NOTHING, db_column='id_producto')
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    cantidad = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ING_PROD'
"""
class ProdMenu(models.Model):
    id_menu = models.ForeignKey(Producto, on_delete=models.CASCADE, related_name='id_menu')
    id_prod = models.ForeignKey(Producto, on_delete=models.CASCADE, related_name='id_prod')
    cantidad = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROD_MENU'
        unique_together = (('id_menu', 'id_prod'),)
"""

class Facturacli(models.Model):
    numero = models.PositiveIntegerField(primary_key=True)
    fecha = models.CharField(max_length=10)
    concepto = models.CharField(max_length=30, blank=True, null=True)
    t_impuesto = models.CharField(max_length=10, blank=True, null=True)
    coste = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FACTURACLI'


class Pedidocli(models.Model):
    numero = models.IntegerField()
    fecha_pedido = models.DateTimeField(auto_now_add=True,blank=True)
    fecha_entrega = models.CharField(max_length=10, blank=True, null=True)
    lugar = models.CharField(max_length=100, blank=True, null=True)
    n_comensales = models.IntegerField(blank=True, null=True)
    t_evento = models.CharField(max_length=15, blank=True, null=True)
    t_servicio = models.CharField(max_length=15, blank=True, null=True)
    requisitos = models.CharField(max_length=30, blank=True, null=True)
    comentarios = models.CharField(max_length=40, blank=True, null=True)
    factura = models.PositiveIntegerField()
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='cliente')
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PEDIDOCLI'



class ProductosPedido(models.Model):
    pedido = models.ForeignKey(Pedidocli, models.DO_NOTHING, db_column='pedido')
    producto = models.ForeignKey(Producto, models.DO_NOTHING, db_column='producto')
    cantidad = models.IntegerField()
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PRODUCTOS_PEDIDO'


class LoginForm(models.Model):
    ROLES = (
        ('ROLE_USER','user'),
        ('ROLE_ADMIN','admin'))
    STATE = (
        ('ALTA','alta'),
        ('BAJA','baja'))

    nombre = models.CharField(max_length=50, blank=None,null=False)
    apellido1 = models.CharField(max_length=50, blank=None,null=False)
    apellido2 = models.CharField(max_length=50)
    razonSocial = models.CharField(max_length=50)
    nif_cif = models.CharField(max_length= 18)

